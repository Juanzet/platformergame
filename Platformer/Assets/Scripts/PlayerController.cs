﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Variables Publicas
    public float speed;
    public float jumpForce;
    public int maxJump = 2;
    public int currentJump = 0;
    public LayerMask capFloor;

    //Variables Privadas
    private bool isGrounded = true;
    private float horizontalStrafe;
    private Rigidbody2D rb2D;
    private BoxCollider2D bc2D;
    private int remainingJumps;
    private bool lookingRight = true;

   


    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        bc2D = GetComponent<BoxCollider2D>();

    }

    private void Start()
    {
        remainingJumps = maxJump; 
    }


    void Update()
    {
        ProcessMovement();

        if (isInTheFloor())
        {
            remainingJumps = maxJump;
        }

        if (Input.GetKeyDown(KeyCode.Space) && remainingJumps > 0)
        {
            remainingJumps--;
            rb2D.velocity = new Vector2(rb2D.velocity.x, 0f);
            rb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
        
    }

    void FixedUpdate()
    {

        rb2D.velocity = new Vector2(horizontalStrafe, rb2D.velocity.y);
    }

    bool isInTheFloor()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(bc2D.bounds.center, new Vector2(bc2D.bounds.size.x, bc2D.bounds.size.y), 0f, Vector2.down, 0.2f, capFloor);
        return raycastHit.collider != null;
    }

    void ProcessMovement()
    {
        horizontalStrafe = Input.GetAxis("Horizontal") * speed;

        horizontalStrafe *= Time.deltaTime;

        CheckOrientation(horizontalStrafe);

        transform.Translate(new Vector3(horizontalStrafe, 0, 0));
    }
    void CheckOrientation(float horizontalStrafe)
    {
        if ((lookingRight = true && horizontalStrafe < 0) || (lookingRight = false && horizontalStrafe > 0))
        {
            lookingRight = !lookingRight;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }
}

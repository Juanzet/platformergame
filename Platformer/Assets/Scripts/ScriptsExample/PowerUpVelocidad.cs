﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpVelocidad : MonoBehaviour
{
    private int cont;
    public float rapidezDesplazamiento;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            other.gameObject.SetActive(false);
            rapidezDesplazamiento = rapidezDesplazamiento * 1.2f;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{
    public float dashVelocidad;
    public float saltoVel;
    bool estaDasheando;
    Rigidbody rb;
    public GameObject dashEfecto;
    Vector3 movimientoDireccion;


     void Start()
    {
        rb = GetComponent<Rigidbody>();  
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            estaDasheando = true;
    }

    private void FixedUpdate()
    {
        if (estaDasheando)
            Dasheando();
    }

    private void Dasheando()
    {
        rb.AddForce(transform.forward * dashVelocidad, ForceMode.Impulse);
        estaDasheando = false;

        GameObject efecto = Instantiate(dashEfecto, Camera.main.transform.position, dashEfecto.transform.rotation);
        efecto.transform.parent = Camera.main.transform;
        efecto.transform.LookAt(transform);

    }

  
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public Rigidbody rb;
    public float saltoVel;
    private bool enElPiso = true;
    public int cantSaltos = 2;
    public int saltoActual = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
     
    void Update()
    {
        if (Input.GetButtonDown("Jump") && (enElPiso ||  cantSaltos > saltoActual)) 
        {
            rb.velocity = new Vector3(0f, saltoVel, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * saltoVel, ForceMode.Impulse);
            enElPiso = false;
            saltoActual++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        enElPiso = true;
        saltoActual = 0;
    }
}
